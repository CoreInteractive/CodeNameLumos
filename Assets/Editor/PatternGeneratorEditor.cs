﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
/// <summary>
/// NOTES FOR UPGRADE
/// need: break up a pattern into parts
/// on reader side have a symbol to create new spawn render
/// on this side maybe list of int[,] gotta see how to store properly/// 
/// undo button** BUILT AND TESTED
/// want: preview with line
/// resize boxes with window
/// 
/// </summary>
public class PatternGeneratorEditor : EditorWindow
{

    enum Modes {Sizing, Planing, FileCreation}
    Modes _progression = Modes.Sizing;

    //variables
    int _xSize = 16;
    int _ySize = 16;
    int _currentPos;
    string _name = "Enter Name";
    string _path = "EMPTY";
    string _spell = "SpellName";
    int[,] Grid;
    // want  to store last 10 pairs;
    List<int> _xundoList= new List<int>();
    List<int> _yundoList = new List<int>();

    [MenuItem("Window/Custom Windows/PatternGenerator")]
    public static void ShowWindow()
    {
        EditorWindow window = GetWindow<PatternGeneratorEditor>("PatternGenerator");
        window.minSize = new Vector2(870f, 890f);
        window.maxSize = window.minSize;
        
    }
    private void OnGUI()
    {
        
        if (_progression == Modes.Sizing)
        {
            _xSize = EditorGUILayout.IntField("X Size", _xSize);
            _ySize = EditorGUILayout.IntField("Y size", _ySize);
            FixZeros();

            if (GUILayout.Button("Make Grid"))
            {
                Grid = new int[_xSize, _ySize];
                for (int y = 0; y < _ySize; y++)
                {
                    for (int x = 0; x < _xSize; x++)
                    {
                        Grid[x, y] = -1;//instantiates 
                    }
                }
                _progression = Modes.Planing;
                _currentPos = 0;
            }

            GUILayout.Label("Preview NON-EDITABLE");

            MakeGrid(false);

        }


        if(_progression == Modes.Planing)
        {
            GUILayout.Label("Choose the Loaction you want to be the " + _currentPos + " in the order");
            MakeGrid(true);
            GUILayout.Space(2);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save Grid"))
            {
                _progression = Modes.FileCreation;
                _currentPos = 0;
            }
            if (GUILayout.Button("Delete Grid"))
            {
                _progression = Modes.Sizing;
                _currentPos = 0;
            }
            if (GUILayout.Button("Undo"))
            {                
              
                Undo();
            }
            GUILayout.EndHorizontal();
        }


        if (_progression == Modes.FileCreation)
        {

            GUILayout.Label("Enter Informaton for the file.");            

            _name = EditorGUILayout.TextField("Enter Name", _name);

            

            //_spell = EditorGUILayout.TextField("Spell Name", _spell);

            if (GUILayout.Button("Choose File Path"))
            {
                _path = EditorUtility.OpenFolderPanel("Files", "Files", "");
            }
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save File") && _path != "EMPTY" && _name != "Enter Name")
            {
                File.WriteAllText(_path + @"\" + _name + ".txt", "");
                FormatGrid(_path + @"\" + _name + ".txt");

                _progression = Modes.Sizing;
            }

            if (GUILayout.Button("Cancel"))
            {
                _progression = Modes.Sizing;
                _currentPos = 0;
            }
            GUILayout.EndHorizontal();
            AssetDatabase.Refresh();
            }
    }

    private void FixZeros()
    {
        if (_xSize <= 0)
        {
            _xSize = 1;
        }
        if (_ySize <= 0)
        {
            _ySize = 1;
        }
    }

    void MakeGrid(bool _editable)
    {
        for (int y = 0; y < _ySize; y++)
        {
            GUILayout.BeginHorizontal();
            for (int x = 0; x < _xSize; x++)
            {
                if (_editable) { MakeGridButton(x, y); }
                else { FillPreview(); }
            }
            GUILayout.EndHorizontal();
        }
    }

    void FormatGrid(string _path)
    {
        string _foramtedString = _xSize + ";" + _ySize+ "\n";
        for (int y = 0; y < _ySize; y++)
        {
            for (int x = 0; x < _xSize; x++)
            {
                _foramtedString +=  Grid[x,y] + ",";
            }
            
        }
        File.WriteAllText(_path, _foramtedString);
    }

    void FillPreview()
    {
        GUILayout.Button("", GUILayout.Height(45), GUILayout.Width(45));
    }
    void Undo()
    {
        int temp = _yundoList.Count-1;
        Grid[_xundoList[temp], _yundoList[temp]] = -1;
        _xundoList.RemoveAt(temp);
        _yundoList.RemoveAt(temp);
        _currentPos--;
    }
    void ChecKUndoSize()
    {
        if (_xundoList.Count > 10)
        {
            _xundoList.RemoveAt(0);
            _yundoList.RemoveAt(0);
        }
    }
    void MakeGridButton(int x, int y)
    {
        if (Grid[x, y] == -1)
        {
            if (GUILayout.Button("", GUILayout.Height(50), GUILayout.Width(50)))
            {
                Grid[x, y] = _currentPos;
                _xundoList.Add(x);
                _yundoList.Add(y);
                ChecKUndoSize();
                _currentPos++;
            }
        }
        else
        {
            GUILayout.Button(Grid[x, y].ToString(), GUILayout.Height(50), GUILayout.Width(50));
        }
    }
}