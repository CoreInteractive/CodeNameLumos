﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellSelect : MonoBehaviour {

    public bool readyToFire;
    public GameObject PatternObject;

    private GameObject glowCapsule;
    private GameObject Aimline;

    [System.Serializable]
    public enum spellType { Lightning, FireBomb, flamethrower, shield };
    public spellType spellTypes;
    public SteamVR_TrackedObject controller;
    public float boltime = 1.0f;
    private AudioSource sound;

    public GameObject boltPrefab;
    public GameObject flameStrike;
    public GameObject flamethrower;
    public GameObject shield;

    public GameObject shieldspawn1, shieldspawn2;
    // Use this for initialization
    void Start()
    {
        glowCapsule = transform.GetChild(1).gameObject;
        Aimline = transform.GetChild(2).gameObject;
        sound = transform.GetComponent<AudioSource>();


    }

    private GameObject ReturnSpell(spellType ST)
    {
        GameObject spell = null;
        switch (ST)
        {
            case spellType.Lightning:
                spell = boltPrefab;
                boltime = 1f;
                break;
            case spellType.FireBomb:
                spell = flameStrike;
                boltime = 2f;
                break;
            case spellType.flamethrower:
                spell = flamethrower;
                boltime = 3f;
                break;

            case spellType.shield:
                spell = shield;
                shieldspawn1.SetActive(true);
                boltime = 5f;
                break;
            default:
                spell = null;
                break;
        }
        print("returning spell");
        return spell;
    }

    private void FixedUpdate()
    {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);
        if (readyToFire)
        {
            PatternObject.SetActive(true);
            glowCapsule.SetActive(true);
            Aimline.SetActive(true);
            transform.GetChild(0).gameObject.SetActive(false);

            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.up, out hit))
            {
                Debug.DrawLine(transform.position, hit.transform.position, Color.red);
            }

            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                sound.Play();
                print("fire");
                glowCapsule.SetActive(false);
                Aimline.SetActive(false);
                GameObject bolt = Instantiate(ReturnSpell(spellTypes), transform.GetChild(0).transform);
                bolt.transform.localPosition = Vector3.zero;
                GameObject endpoint = bolt.transform.GetChild(1).gameObject;
                RaycastHit hitt;
                if (Physics.Raycast(transform.position, transform.up, out hitt))
                {
                    Collider target = hit.collider; // What did I hit?
                    float distance = hit.distance; // How far out?
                    print(target.gameObject);
                    Vector3 location = hit.point; // Where did I make impact?
                    endpoint.transform.position = location;
                    if (target.tag == "Mob")
                    {
                        target.GetComponent<MeatballController>().DestroyMeatball();
                    }

                }
                StartCoroutine(killbolt(bolt));
                readyToFire = false;

                transform.GetChild(0).gameObject.SetActive(true);

            }
        }
    }

    private IEnumerator killbolt(GameObject tokill)
    {
        yield return new WaitForSeconds(boltime);
        Destroy(tokill);
    }
    private IEnumerator ShowPattern()
    {
        yield return new WaitForSeconds(1.0f);
        PatternObject.SetActive(true);
    }
}
