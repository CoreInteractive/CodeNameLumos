﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {
    public GameObject TargetGroup;
    public GameObject Target;
    public GameObject AsteroidBits;
    AsteroidSpawnSystem _ass;
    public float speed;
    private void Start()
    {
        TargetGroup = GameObject.Find("CityTargets");
        float ran = Random.Range(0, TargetGroup.transform.childCount);
        Target = TargetGroup.transform.GetChild((int)ran).gameObject;
        _ass = GameObject.FindObjectOfType<AsteroidSpawnSystem>();

    }
    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, Target.transform.position, speed);
    }

    public void DestroyAsteroid()
    {
        Instantiate(AsteroidBits);
        //add code here to remove this game obj from the Asteroid list 
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)// have to handle both hitting city and hitting lazer
    {
        print("hit " + other.name);
        if (other.tag == "Laser")
        {
            Destroy(other.GetComponent<Lazer>());
            Destroy(other.gameObject);
            _ass.removeAsteroid(this.gameObject);
            DestroyAsteroid();
            
        }
        else if (other.tag == "City")
        {
            //burn city or something
        }
    }

    public void RemoveAsteroid()
    { 
        _ass.removeAsteroid(this.gameObject);
        AsteroidBits.transform.position = transform.position;
        DestroyAsteroid();
    }
}
