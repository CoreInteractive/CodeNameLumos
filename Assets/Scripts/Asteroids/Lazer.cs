﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VolumetricLines.Utils;

public class Lazer : MonoBehaviour {

    public GameObject Target;
    VolumetricLines.VolumetricLineBehavior _lazer;
    
	// Use this for initialization
	void Start () {
        _lazer = transform.GetComponentInChildren<VolumetricLines.VolumetricLineBehavior>();
        _lazer.StartPos = Vector3.zero;
        StartCoroutine(destroyAsteroids());
    }
	
	// Update is called once per frame
	void Update () {
        
        _lazer.EndPos = transform.InverseTransformPoint(Target.transform.position);     
    }

    IEnumerator destroyAsteroids()
    {
        yield return new WaitForSeconds(.8f);

        Target.GetComponent<Asteroid>().RemoveAsteroid();
        Destroy(gameObject);
    }
}
