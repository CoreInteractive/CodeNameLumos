﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleLauncherManager : MonoBehaviour {

    // add lazers 
    // add which lazers will fire to
    public GameObject[] MissileGroup;
    RotateTurret CurrentTurret;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}

    public void SelectLauncher(GameObject Target)
    {

        int _randomLauncher = (int)Random.Range(0, transform.childCount);
        CurrentTurret = MissileGroup[_randomLauncher].GetComponent<RotateTurret>();
        CurrentTurret.AsteroidTarget = Target;
    }

   public void Attack()
    {
        //lazers or missles>
        CurrentTurret.Fire();
    }
}
