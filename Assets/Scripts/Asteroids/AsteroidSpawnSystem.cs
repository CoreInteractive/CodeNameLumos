﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawnSystem : MonoBehaviour {
    public GameObject AsteroidPrefab;
    public int MaxNumberOfAsteroids;
    public int CurrentNumberOfAsteroids;
   public List<GameObject> Asteroids;
    public GameObject TargetedAsteroid;
    public MissleLauncherManager MissileManager;
    
	// Use this for initialization
	void Start () {
        Asteroids = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
        Spawntimer();
	}
    public void Spawntimer()
    {
        if (CurrentNumberOfAsteroids < MaxNumberOfAsteroids)
        {
            print("Spawing Asteroid");
            CurrentNumberOfAsteroids++;
            int ran =(int) Random.Range(2, 5);
            StartCoroutine(wait(ran));
            
        }
    }

    void ChooseSpawnpoint()
    {
        int childCount = transform.childCount;
        int ran = (int) Random.Range(0, childCount);
        GameObject Spawnpoint = transform.GetChild(ran).gameObject;
        Spawn(Spawnpoint);
        Destroy(Spawnpoint);
    }

    void Spawn(GameObject point)
    {
        GameObject temp = Instantiate(AsteroidPrefab, point.transform.position, point.transform.rotation);
        Asteroids.Add(temp);

        
    }
    private IEnumerator wait(int time)
    {
        yield return new WaitForSeconds(time);
        ChooseSpawnpoint();
    }

    public void SelectAsteroid()
    {
        //int ran = (int) Random.Range(0, CurrentNumberOfAsteroids);
        MissileManager.SelectLauncher(Asteroids[0]);

        //Asteroids[(int)ran].GetComponent<Asteroid>().DestroyAsteroid();
        //Asteroids.Remove(Asteroids[(int)ran]);
        //CurrentNumberOfAsteroids--;
        
    }
    public void FireTurret()
    {
        MissileManager.Attack();
    }

    public void removeAsteroid(GameObject ast)
    {
        Asteroids.Remove(ast);
        CurrentNumberOfAsteroids--;

    }
}
