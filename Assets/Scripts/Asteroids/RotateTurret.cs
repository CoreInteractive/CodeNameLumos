﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTurret : MonoBehaviour {

    public GameObject AsteroidTarget;
    private GameObject MisslePivot;
    public GameObject LaserPrefab;
    public float speed;
	// Use this for initialization
	void Start () {
        MisslePivot = this.transform.GetChild(0).gameObject;	
	}
	
	// Update is called once per frame
	void Update () {
		if (AsteroidTarget != null)
        {
            Vector3 TargetDir = AsteroidTarget.transform.position - MisslePivot.transform.position;
            float step = speed * Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(MisslePivot.transform.forward, TargetDir, step, .0f);
            MisslePivot.transform.rotation = Quaternion.LookRotation(newDir);
        }
	}
    public void Fire()
    {
        print("Firing");
        GameObject laser=Instantiate(LaserPrefab);
        laser.transform.position = transform.position;
        laser.SetActive(true);
        laser.GetComponent<Lazer>().Target = AsteroidTarget;

        AsteroidTarget = null;
    }
}
