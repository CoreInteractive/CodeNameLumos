﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBits : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        StartCoroutine(Bits());
    }

    IEnumerator Bits()
    {
        yield return new WaitForSeconds(1.5f);

        Destroy(gameObject);
    }
}
