﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour {
    public GameObject Starttext;
    bool gameStart = false;
    public GameObject EnemyManager;
    public GameObject PaternStuff;
    public HandDetection wand;
	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        Starttext.SetActive(false);
        EnemyManager.SetActive(true);
        PaternStuff.SetActive(true);
        this.gameObject.SetActive(false);
        wand.enabled = true;
    }


}
