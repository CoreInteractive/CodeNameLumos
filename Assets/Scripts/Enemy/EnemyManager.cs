﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {
    
    public enum State {Build, Building, Running, Wait}
    public State WaveState = State.Building;
    public int _waveMax;
    public BoxHealth wall;
    [System.Serializable]
    public class _path
    {
        public Transform[] path;
    }
    public _path[] paths;
    
    public GameObject EnemyPrefab;
    public GameObject EndGame;
    public List<MeatballController> Enemys;

    public float speed;


    //wave stuff 
    int _waveNumber;
    public int EnemyCountThisWave = 3;
    public int EnemyAddPerWave = 2;
    public TextMesh waveDisplay;
    public bool hasLost = false;

    private void Start()
    {
        /*
        int randomPath = Random.Range(0, paths.Length-1);
        MeatballController newMeatball = Instantiate(EnemyPrefab, paths[randomPath].path[0].position, paths[randomPath].path[0].rotation).GetComponent<MeatballController>(); ;
        newMeatball.StartMoving(paths[randomPath].path);
        */
    }

    private void Update()
    {
        if (_waveNumber > _waveMax)
        {
            //endgame
            EndGame.SetActive(true);
            waveDisplay.text = " Game Over";
            return;
        }
        waveDisplay.text = "Wave: " + _waveNumber;

        if (WaveState == State.Build)
        {
            _waveNumber++;
            StartCoroutine(SpawnWave());
            Enemys = new List<MeatballController>();
            WaveState = State.Building;
        }
        if (WaveState == State.Running && Enemys.Count == 0)
        {
            WaveState = State.Build;
            EnemyCountThisWave = EnemyCountThisWave + EnemyAddPerWave;
        }
    }

    public void Lose()
    {
        hasLost = true;
        EnemyCountThisWave = 3;
        _waveNumber = 0;
        WaveState = State.Wait;

        wall.wallHealth = 10;
        wall.GetComponent<BoxCollider>().enabled = true;
        wall.ResetBoxes();

        EndGame.SetActive(true);
        //StartCoroutine(WaveDelay());
    }

    IEnumerator WaveDelay()
    {
        yield return new WaitForSeconds(10);
        hasLost = !hasLost;
        WaveState = State.Build;
    }

    IEnumerator SpawnWave()
    {
        for (int i = 0; i < EnemyCountThisWave; i++)
        {
            int randomPath = UnityEngine.Random.Range(0, paths.Length - 1);
            MeatballController newMeatball = Instantiate(EnemyPrefab, paths[randomPath].path[0].position, paths[randomPath].path[0].rotation).GetComponent<MeatballController>(); ;
            newMeatball.StartMoving(paths[randomPath].path);
            newMeatball.speed = speed;
            newMeatball.enemyManager = GetComponent<EnemyManager>();
            Enemys.Add(newMeatball);

            yield return new WaitForSeconds(2);
        }
        WaveState = State.Running;
    }
}
