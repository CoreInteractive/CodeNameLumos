﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeatballController : MonoBehaviour {
    public float Damage;
    Transform[] _path;
    int _currentPathSpot = 1;
    public GameObject Explosion;
    public bool dead= false;
    //[HideInInspector]
    public EnemyManager enemyManager;
    public float speed = 1;
    void Update()
    {

        if (!dead) {
            float _speed = speed * Time.deltaTime;
            transform.LookAt(_path[_currentPathSpot]);
            transform.position = Vector3.MoveTowards(transform.position, _path[_currentPathSpot].position, speed);
            if (Vector3.Distance(transform.position, _path[_currentPathSpot].position) < .5f)
            {
                _currentPathSpot++;
                if (_currentPathSpot == _path.Length)
                {
                    DestroyMeatball();
                    if (!enemyManager.hasLost)
                    {
                        enemyManager.Lose();
                    }
                }

            }
            if (enemyManager.hasLost)
            {
                enemyManager.Enemys.Remove(GetComponent<MeatballController>());
                Destroy(gameObject);
            }
        }
        
    }

    public void DestroyMeatball()
    {
        enemyManager.Enemys.Remove(GetComponent<MeatballController>());
        //run animaton for explosion and then die;
        dead = true;
        transform.GetChild(0).GetComponent<Animation>().Play("Anim_Death") ;
        StartCoroutine(wait());
        GameObject temp = Instantiate(Explosion, transform);
        temp.transform.localPosition = Vector3.zero;
    }

    public void StartMoving(Transform[] _pathTemp)
    {
        _path = _pathTemp;
    }
    public IEnumerator wait()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
