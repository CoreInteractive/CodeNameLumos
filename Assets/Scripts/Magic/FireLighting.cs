﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLighting : MonoBehaviour {
    public bool readyToFire;
    public GameObject boltPrefab;
    //private GameObject glowCapsule;
    private GameObject Aimline;
    public SteamVR_TrackedObject controller;
    DataCollection data;
    public float boltime = 1.0f;
    private AudioSource sound;
        	// Use this for initialization
            //may need a new spotter
        
	void Start () {
        //glowCapsule = transform.GetChild(1).gameObject;
        Aimline = transform.GetChild(1).gameObject;
        sound = transform.GetComponent<AudioSource>();
        data = FindObjectOfType<DataCollection>();

    }
    private void FixedUpdate()
    {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);
        if (readyToFire)
        {
            //glowCapsule.SetActive(true);
            Aimline.SetActive(true);
            transform.GetChild(0).gameObject.SetActive(false);
           
             RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.up, out hit))
            {
                Debug.DrawLine(transform.position, hit.transform.position, Color.red) ;
            }
            
            if (device.GetPressDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                sound.Play();
                print("fire");
                //glowCapsule.SetActive(false);
                Aimline.SetActive(false); 
                GameObject bolt = Instantiate(boltPrefab, transform.GetChild(0).transform);
                bolt.transform.localPosition = Vector3.zero;
                GameObject endpoint = bolt.transform.GetChild(1).gameObject;
                RaycastHit hitt;
                if (Physics.Raycast(transform.position, transform.up,out hitt))
                {
                    Collider target = hit.collider; // What did I hit?
                    float distance = hit.distance; // How far out?
                    print(target.gameObject);
                    Vector3 location = hit.point; // Where did I make impact?
                    endpoint.transform.position = location;
                    if (target.tag == "Mob")
                    {
                        target.GetComponent<MeatballController>().DestroyMeatball();
                    }
                    
                }
                StartCoroutine(killbolt(bolt));
                readyToFire = false;

                transform.GetChild(0).gameObject.SetActive(true);
                data.StartNewPattern();
            }
        }
    }
   
    private IEnumerator killbolt(GameObject tokill)
    {
        yield return new WaitForSeconds(boltime);
        Destroy(tokill);
    }
}
