﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTheFlyCast : MonoBehaviour {
    public SteamVR_TrackedController Controller;
    public GameObject PatternDetection;
    public GameObject LookAtTarget;
    public GameObject WandSparks;
    public bool Line=false;

    public PatternReaderV2 PRV = new PatternReaderV2();
    private int[,] wandGrid;
    public int count = 0;
    bool DrawingSpell = false;
    GameObject SpellGrid;
    // Use this for initialization
    void Start () {
        ResetGrid();
//PRV = FindObjectOfType<PatternReaderV2>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Controller.triggerPressed)
        {
            if (!DrawingSpell) {
               SpellGrid = Instantiate(PatternDetection);
                PRV = SpellGrid.transform.GetChild(1).GetComponent<PatternReaderV2>();
                SpellGrid.transform.parent = this.transform;
                SpellGrid.transform.localPosition =new Vector3(0,1,0);
                SpellGrid.transform.parent = null;
                SpellGrid.transform.name = "Casting Circle";
                SpellGrid.SetActive(true);
                SpellGrid.transform.LookAt(LookAtTarget.transform.position);
                WandSparks.SetActive(true);
                WandSparks.GetComponent<ParticleSystem>().Play();
                DrawingSpell = true;
               
            }
        }
            
            
       if (!Controller.triggerPressed)
        {
            count = 0;
            DrawingSpell = false;
            WandSparks.SetActive(false);
            WandSparks.GetComponent<ParticleSystem>().Stop();
            if (SpellGrid!=null)
            {
                Destroy(SpellGrid);
            }
            PatternDetection.SetActive(false);
            ResetGrid();
            PRV = null;
        }
	}
     void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<XY>() )
        {

            int x = other.gameObject.GetComponent<XY>().x;
            int y = other.gameObject.GetComponent<XY>().y;
            wandGrid[x, y] = count;
            print(x + " " + y + " " + wandGrid[x, y]);
            count++;

            if (Line)
                PRV.DrawLine(other.transform);

            PRV.ComparePattern(wandGrid);

        }
    }

    private void ResetGrid()
    {
        wandGrid = new int[5, 5];
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                wandGrid[i, j] = -1;
            }
        }
    }
}
