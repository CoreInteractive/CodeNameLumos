﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrink : MonoBehaviour {
    public bool startShrink = false;
    public float shrinkSize;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(startShrink)
        {
            this.transform.localScale = new Vector3(shrinkSize,shrinkSize, shrinkSize);
            startShrink = false;
        }
	}
}
