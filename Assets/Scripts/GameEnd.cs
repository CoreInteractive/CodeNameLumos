﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnd : MonoBehaviour {
    public TextMesh avgTime;
    public TextMesh avgError;
    EnemyManager enemyManger;
    public GameObject StartGame;
    public GameObject PatternStuff;
    public DataCollection data;
	// Use this for initialization
	void Start () {
        data = FindObjectOfType<DataCollection>();

        PatternStuff.SetActive(false);
        avgError.text = "Total Errors: " +data.erroorcount.ToString();
        avgTime.text= "Average Time per Pattern: " + data.AverageTime().ToString();
    }
    
}
