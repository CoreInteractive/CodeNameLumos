﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxHealth : MonoBehaviour {
    public float wallHealth;
    public float force;
    public GameObject boxesPrefab;

    GameObject CurrentBoxes;
    EnemyManager _manager;

    // Use this for initialization
    void Start () {
        _manager = FindObjectOfType<EnemyManager>();

        CurrentBoxes = Instantiate(boxesPrefab, transform.parent);
        CurrentBoxes.transform.localPosition = Vector3.zero;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Mob")
        {
            other.GetComponent<MeatballController>().DestroyMeatball();
            wallHealth -=other.GetComponent<MeatballController>().Damage;
            if (wallHealth <= 0)
            {
                GetComponent<BoxCollider>().enabled = false;
                print("Lose");
            }
        }
    }

    public void ResetBoxes()
    {
        Destroy(CurrentBoxes);
        CurrentBoxes = Instantiate(boxesPrefab, transform.parent);
        CurrentBoxes.transform.localPosition = Vector3.zero;
    }
}
