﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataCollection : MonoBehaviour {
    public int patternCount;
    public float averageTime;
    public int erroorcount;
    public float errorsPerPattern;
    public bool timing;
    public List<float> Patternstime;
    public float timer = 0f;
    class PatternData
    {
        public int errorCount;
       public float time;

    }
    // Use this for initialization

    void Start () {
        Patternstime = new List<float>();
	}

    private void FixedUpdate()
    {
        if(timing)
             timer += Time.deltaTime;
    }
    public void Stoptimer()
    {
        timing = false;
        Patternstime.Add(timer);
        timer = 0;
    
    }
    
    public void StartNewPattern()
    {

        timer = 0.0f;
        timing = true;
    }

    public float AverageTime()
    {
        for (int i = 0; i < Patternstime.Count; i++)
        {
            averageTime += Patternstime[i];
        }
        averageTime = averageTime / Patternstime.Count;
        return averageTime;
    }
    //public float AverageError()
    //{
    //    for (int i = 0; i < patternCount; i++)
    //    {
    //        errorsPerPattern += PatternsThisSession[i].errorCount;
    //    }
    //    //errorsPerPattern = errorsPerPattern / patternCount;
    //    return errorsPerPattern;
    //}
    public void AddError()
    {
        erroorcount++;
    }
}
