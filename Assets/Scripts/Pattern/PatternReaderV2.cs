﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
/// <summary>
/// Modified pattern reader for On the fly casting 
/// </summary>
public class PatternReaderV2 : MonoBehaviour {

    class PointHolder
    {
        public int x;
        public int y;
    }

    public int xSize = 0;
    public int ySize = 0;
    public TextAsset[] patterns;
    //public Transform gridParent;

    //AsteroidSpawnSystem _ass;
    //MissleLauncherManager _mlm;

    int _posInCurPattern = 0;
    GameObject[,] grid;
    int[,] patternGrid;
    List<PointHolder> patternOrder;
    
    //public GameObject currentGridPoint;
    [Header("Options")]
    [Tooltip("Pattern as it is made ")]
    public bool RenderLineOnUser;
    LineRenderer userLine;
    [Tooltip("Shows the pattern to follow")]
    public bool GuideLineRenderer;
    LineRenderer GuideLine;
    public FireLighting wand;
    private AudioSource sound;
    //public DataCollection data;
    private Transform lastGridLocation;
    List<int[,]> patternlist= new List<int[,]>();
    List<string> spellNames = new List<string>();
    private void Update()
    {
        //Debug.DrawRay(userLine.transform.position, userLine.transform.forward, Color.red);
        //Debug.DrawRay(userLine.transform.position, userLine.transform.right, Color.green);
        //Debug.DrawRay(userLine.transform.position, userLine.transform.up, Color.blue);
    }

    private void Start()
    {

        //sets up the lines ad finds them
        userLine = transform.Find("Lines").Find("UserLine").GetComponent<LineRenderer>();
        //GuideLine = transform.Find("Lines").Find("GuideLine").GetComponent<LineRenderer>();
      
        sound = this.GetComponent<AudioSource>();
        //checks if the User Progress Line should display
        //if (!RenderLineOnUser)
        //{// might be re implemented for tutorials or for easier spell display 
        //    Destroy(userLine.gameObject);
        //}
        wand = GameObject.FindObjectOfType<FireLighting>();
        ////same but if the overal pattern should display
        //if (!GuideLineRenderer)
        //{
        ////    Destroy(GuideLine.gameObject);
        //}
        TestAllPatterns(patterns);
        //if(gridParent == null || gridParent == transform)
        //{
        //    gridParent = transform.Find("Grid");
        //}

        //starts the the pattern part
        //find the gameObjects that are in the grid and puts them in a 2D array by x,y 
        //FillGrid(gridParent);
        //reads the pattern from the file and gets the order based on a 2D array
        FillPatterns();
        //moves the pattern forward one
        //ForwardInPattern();
        //data.StartNewPattern();
    }
    #region Legacy
    //moves the pattern forward one
    //void ForwardInPattern()
    //{
    //     currentGridPoint = grid[patternOrder[_posInCurPattern].x, patternOrder[_posInCurPattern].y];
    //    currentGridPoint.tag = "Correct";

    //    userLine.positionCount++;
    //    userLine.SetPosition(userLine.positionCount - 1, grid[patternOrder[_posInCurPattern].x, patternOrder[_posInCurPattern].y].transform.position + new Vector3(-.01f, 0.0f, 0));

    //    if (_posInCurPattern == 0)
    //    {
    //        userLine.positionCount = 2;
    //    }
    //}
    //is sent by object on the "hand" and calls this method to move forward
    //public void PointComplete()
    //{
    //    print("point");
    //    grid[patternOrder[_posInCurPattern].x, patternOrder[_posInCurPattern].y].tag = "Incorrect";
    //    _posInCurPattern++;
    //    if (_posInCurPattern > patternOrder.Count-1)
    //    {
    //        _posInCurPattern = 0;
    //        wand.readyToFire = true;
    //        sound.Play();
    //        NextPattern();
    //        data.Stoptimer();
    //    }
    //    else
    //    {
    //        print("foward");
    //        ForwardInPattern();
    //    }

    //}
    //Moves the program to the next pattern
    //void NextPattern()
    //{
    //    _posInPatterns = UnityEngine.Random.Range(0, patterns.Length);

    //    //reads the pattern from the file and gets the order based on a 2D array
    //    FillNewPattern(patterns[_posInPatterns]);
    //    //moves the pattern forward one
    //    ForwardInPattern();
    //   // _ass.SelectAsteroid();
    //    //_mlm.Attack();

    //    }
    //find the gameObjects that are in the grid and puts them in a 2D array by x,y 
    //void FillGrid(Transform _parent)
    //{
    //    grid = new GameObject[xSize, ySize];
    //    patternGrid = new int[xSize, ySize];

    //    for (int _y = 0; _y < ySize; _y++)
    //    {
    //        for (int _x = 0; _x < xSize; _x++)
    //        {
    //            grid[_x,_y] = _parent.GetChild(_y).GetChild(_x).gameObject;
    //        }
    //    }
    //}
    //reads the pattern from the file and gets the order based on a 2D array
#endregion

    void FillPatterns()
    {
        
        
        int posInFile = 1;

        for(int i = 0; i <patterns.Length;i++)
        {
            patternGrid = new int[xSize, ySize];
            TextAsset _pattern = patterns[i];
            string[] _gridParts = _pattern.text.Split(',', ':', '\n');
            for (int _y = 0; _y < ySize; _y++)
            {
                for (int _x = 0; _x < xSize; _x++)
                {
                    patternGrid[_x, _y] = Int32.Parse(_gridParts[(posInFile)]);
                    posInFile++;
                }
            }
            patternlist.Add(patternGrid);
        }



    }

    public void DrawLine(Transform target)
    {
        if (_posInCurPattern == 0)
        {
            _posInCurPattern++;
            lastGridLocation = target;
            userLine.SetPosition(0, target.position);
        }
        else
        {
            _posInCurPattern++;
            userLine.positionCount++;
            userLine.SetPosition(userLine.positionCount - 1,target.transform.position);// add some z vector
            lastGridLocation = target;// not sure if needed 

            //    if (_posInCurPattern == 0)
            //    {
            //        userLine.positionCount = 2;
            //    }
            //}
        }
    }
    //checks if all the patterns are the right size so no index errors
    void TestAllPatterns(TextAsset[] _patterns)
    {
        int _placeInPatterns = 0;
        string[] _testGridParts;
        foreach (TextAsset _txt in _patterns)
        {
            _testGridParts = _txt.text.Split(',', ':', '\n');
            //checks if the size is the same as the x & y for the whole reader 
            if (!TestSize(_testGridParts[0]))
            {
                //prints and error message and pauses the program so the error doesnt cause index errors
                Debug.LogError("The Grid Size of " + _patterns[_placeInPatterns].name + " is invalid please remove from array to continue");
                _placeInPatterns++;
                Debug.Break();
            }
            else {
                spellNames.Add(_txt.name);
            }
        }
        _placeInPatterns++;
    }
    //checks if the size is the same as the x & y for the whole reader
    public bool TestSize(string _size)
    {
        string[] _xy = _size.Split(';');
        int _fileX = int.Parse(_xy[0]);
        int _fileY = int.Parse(_xy[1]);
        //spellNames.Add(_xy[2]);

        if (_fileX == xSize && _fileY == ySize)
        {
            return true;
        }
        return false;
    }

    public void ComparePattern(int[,] grid2compare)
    {
        print("comparing");
        for (int i = 0; i < patternlist.Count; i++)
        {
            if (Gridcompare(grid2compare, patternlist[i]))
            {
                print(spellNames[i]+ "Detected");
                //spellcomplete
                GameObject.FindObjectOfType<SpellController>().SpellSelect(spellNames[i]);
            }
            else
            {
                // show error
            }
        }

    }
    bool Gridcompare( int [,] grid1, int [,] grid2)
    {
        for (int y = 0; y < ySize; y++)
        {
            for (int x = 0; x < xSize; x++)
            {
                print(x+" "+y+" " +grid1[x, y] + "  and spell is " + grid2[x, y]);
                if (grid1[x, y] != grid2[x, y])
                {
                    return false;
                    

                }
            }
        }
        return true;
    }
    //runs throught pattern and sets the line up to display it
    //void DisplayPatternLineGuide()
    //{
    //    GuideLine.positionCount = patternOrder.Count-1;
        
    //    for (int i = 0; i < patternOrder.Count-1; i++)
    //    {
    //        GuideLine.SetPosition(i, grid[patternOrder[i].x, patternOrder[i].y].transform.position);
    //    }
    //}
}
