﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class patternTest : MonoBehaviour {

    PatternReader _reader;

    private void Start()
    {
        _reader = FindObjectOfType<PatternReader>();
    }
    private void OnTriggerEnter(Collider other)
    {
        _reader.PointComplete();
    }
}
