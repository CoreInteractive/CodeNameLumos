﻿//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandDetection : MonoBehaviour {

    public PatternReader _reader;
    public float ErrorThreshold;
    public DataCollection data;

    private void Start()
    {
        _reader = FindObjectOfType<PatternReader>();
    }
    private void OnTriggerEnter(Collider other)
    {
        GameObject temp = _reader.currentGridPoint;
        // here we need to seperate correct vs not collisions
        if (other.tag == "Correct")
        {
            print("correct");
            _reader.PointComplete();
        }
        else if (other.tag == "Incorrect")
        {
            CalculateError(temp, other.gameObject);
        }
       
    }

    void CalculateError(GameObject targetWanted,GameObject targetHit )
    {
        float MagofError = Vector3.Distance(targetWanted.transform.position, targetHit.transform.position);

        if(MagofError> ErrorThreshold)
        {
            print(" the mag of error is" + MagofError);
            print("Error");
            data.AddError();
        }
    }
}
