﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PatternReader : MonoBehaviour {

    class PointHolder
    {
        public int x;
        public int y;
    }

    public int xSize = 0;
    public int ySize = 0;
    public TextAsset[] patterns;
    public Transform gridParent;

    //AsteroidSpawnSystem _ass;
    //MissleLauncherManager _mlm;

    int _posInPatterns = 0;
    int _posInCurPattern = 0;
    GameObject[,] grid;
    int[,] patternGrid;
    List<PointHolder> patternOrder;
    
    public GameObject currentGridPoint;
    [Header("Options")]
    [Tooltip("AFTER completeing a point makes a line from last point.")]
    public bool RenderLineOnUser;
    LineRenderer userLine;
    [Tooltip("Shows path that throught the grid")]
    public bool GuideLineRenderer;
    LineRenderer GuideLine;
    public FireLighting wand;
    private AudioSource sound;
    public DataCollection data;
    private void Update()
    {
        Debug.DrawRay(userLine.transform.position, userLine.transform.forward, Color.red);
        Debug.DrawRay(userLine.transform.position, userLine.transform.right, Color.green);
        Debug.DrawRay(userLine.transform.position, userLine.transform.up, Color.blue);
    }

    private void Start()
    {

        //sets up the lines ad finds them
        userLine = transform.Find("Lines").Find("UserLine").GetComponent<LineRenderer>();
        GuideLine = transform.Find("Lines").Find("GuideLine").GetComponent<LineRenderer>();
        //_ass = FindObjectOfType<AsteroidSpawnSystem>();
        //_mlm = FindObjectOfType<MissleLauncherManager>();
        sound = this.GetComponent<AudioSource>();
        //checks if the User Progress Line should display
        if (!RenderLineOnUser)
        {
            Destroy(userLine.gameObject);
        }
        //same but if the overal pattern should display
        if (!GuideLineRenderer)
        {
            Destroy(GuideLine.gameObject);
        }
        //checks if all the patterns are the right size so no index errors
        TestAllPatterns(patterns);
        //if the grid parent isnt set the program sets it for you
        if(gridParent == null || gridParent == transform)
        {
            gridParent = transform.Find("Grid");
        }

        //starts the the pattern part
        //find the gameObjects that are in the grid and puts them in a 2D array by x,y 
        FillGrid(gridParent);
        //reads the pattern from the file and gets the order based on a 2D array
        FillNewPattern(patterns[_posInPatterns]);
        //moves the pattern forward one
        ForwardInPattern();
        data.StartNewPattern();
    }
    //moves the pattern forward one
    void ForwardInPattern()
    {
         currentGridPoint = grid[patternOrder[_posInCurPattern].x, patternOrder[_posInCurPattern].y];
        currentGridPoint.tag = "Correct";

        userLine.positionCount++;
        userLine.SetPosition(userLine.positionCount - 1, grid[patternOrder[_posInCurPattern].x, patternOrder[_posInCurPattern].y].transform.position + new Vector3(-.01f, 0.0f, 0));

        if (_posInCurPattern == 0)
        {
            userLine.positionCount = 2;
        }
    }
    //is sent by object on the "hand" and calls this method to move forward
    public void PointComplete()
    {
        print("point");
        grid[patternOrder[_posInCurPattern].x, patternOrder[_posInCurPattern].y].tag = "Incorrect";
        _posInCurPattern++;
        if (_posInCurPattern > patternOrder.Count-1)
        {
            _posInCurPattern = 0;
            wand.readyToFire = true;
            sound.Play();
            NextPattern();
            data.Stoptimer();
        }
        else
        {
            print("foward");
            ForwardInPattern();
        }

    }
    //Moves the program to the next pattern
    void NextPattern()
    {
        _posInPatterns = UnityEngine.Random.Range(0, patterns.Length);
        
        //reads the pattern from the file and gets the order based on a 2D array
        FillNewPattern(patterns[_posInPatterns]);
        //moves the pattern forward one
        ForwardInPattern();
       // _ass.SelectAsteroid();
        //_mlm.Attack();

        }
    //find the gameObjects that are in the grid and puts them in a 2D array by x,y 
    void FillGrid(Transform _parent)
    {
        grid = new GameObject[xSize, ySize];
        patternGrid = new int[xSize, ySize];
        
        for (int _y = 0; _y < ySize; _y++)
        {
            for (int _x = 0; _x < xSize; _x++)
            {
                grid[_x,_y] = _parent.GetChild(_y).GetChild(_x).gameObject;
            }
        }
    }
    //reads the pattern from the file and gets the order based on a 2D array
    void FillNewPattern(TextAsset _pattern)
    {
        string[] _gridParts = _pattern.text.Split(',', ':', '\n');
        
        int posInFile = 1;
        for (int _y = 0; _y < ySize; _y++)
        {
            for (int _x = 0; _x < xSize; _x++)
            {
                patternGrid[_x, _y] = Int32.Parse(_gridParts[(posInFile)]);
                posInFile++;
            }
        }

        //find patternOrder
        int _maxNumberInOrder = 0;
        patternOrder = new List<PointHolder>();
        do
        {
            int _prevMax = _maxNumberInOrder;
            for (int _y = 0; _y < ySize; _y++)
            {
                for (int _x = 0; _x < xSize; _x++)
                {

                    if(patternGrid[_x,_y] == _maxNumberInOrder)
                    {
                        patternOrder.Add(new PointHolder());
                        patternOrder[patternOrder.Count - 1].x = _x;
                        patternOrder[patternOrder.Count - 1].y = _y;
                        _maxNumberInOrder++;
                    }
                }
            }
            if (_prevMax == _maxNumberInOrder)
            {
                _maxNumberInOrder = -1;
            }
        } while (_maxNumberInOrder != -1);

        if (GuideLineRenderer)
        {
            DisplayPatternLineGuide();
        }

        if (RenderLineOnUser)
        {
            userLine.positionCount = 0;
            userLine.positionCount = 2;
            userLine.SetPosition(0, grid[patternOrder[0].x, patternOrder[0].y].transform.position);
            userLine.SetPosition(1, grid[patternOrder[1].x, patternOrder[1].y].transform.position);
        }
    }
    //checks if all the patterns are the right size so no index errors
    void TestAllPatterns(TextAsset[] _patterns)
    {
        int _placeInPatterns = 0;
        string[] _testGridParts;
        foreach (TextAsset _txt in _patterns)
        {
            _testGridParts = _txt.text.Split(',', ':', '\n');
            //checks if the size is the same as the x & y for the whole reader 
            if (!TestSize(_testGridParts[0]))
            {
                //prints and error message and pauses the program so the error doesnt cause index errors
                Debug.LogError("The Grid Size of " + _patterns[_placeInPatterns].name + " is invalid please remove from array to continue");
                _placeInPatterns++;
                Debug.Break();
            }
        }
        _placeInPatterns++;
    }
    //checks if the size is the same as the x & y for the whole reader
    public bool TestSize(string _size)
    {
        string[] _xy = _size.Split(';');
        int _fileX = int.Parse(_xy[0]);
        int _fileY = int.Parse(_xy[1]);

        if (_fileX == xSize && _fileY == ySize)
        {
            return true;
        }
        return false;
    }
    //runs throught pattern and sets the line up to display it
    void DisplayPatternLineGuide()
    {
        GuideLine.positionCount = patternOrder.Count-1;
        
        for (int i = 0; i < patternOrder.Count-1; i++)
        {
            GuideLine.SetPosition(i, grid[patternOrder[i].x, patternOrder[i].y].transform.position);
        }
    }
}
