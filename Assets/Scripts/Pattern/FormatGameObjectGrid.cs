﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// NOTES: needs undo button
/// maybe grow objects with size of window
/// </summary>
[ExecuteInEditMode]
public class FormatGameObjectGrid : MonoBehaviour {

    public GameObject[] objects;
    public float SeperationDistance;
    public enum ArrayTypes { FullGrid, OneRow}
    public ArrayTypes ArrayType = ArrayTypes.OneRow;

    public enum FormatType {Position, Name, None, ResetPos, OFF, ON, Invisible, Visible }
    public FormatType Type = FormatType.None;

    public enum xyz { x, y, z}
    public xyz Axis = xyz.x;

    public string NameBase = "";

	void Update ()
    {
        if (Type == FormatType.Name)
        {
            Name(objects);
            Type = FormatType.None;
        }

        if (ArrayType == ArrayTypes.OneRow)
        {
            if (Type == FormatType.Position)
            {
                Postion();
                Type = FormatType.None;
            }

            if (Type == FormatType.ResetPos)
            {
                PosReset(objects);
                Type = FormatType.None;
            }

            if (Type == FormatType.Name)
            {
                Name(objects);
                Type = FormatType.None;
            }

            if (Type == FormatType.OFF)
            {
                OFF(objects);
                Type = FormatType.None;
            }

            if (Type == FormatType.ON)
            {
                ON(objects);
                Type = FormatType.None;
            }
            if (Type == FormatType.Visible)
            {
                Visible(objects);
                Type = FormatType.None;
            }
            if (Type == FormatType.Invisible)
            {
                InVisible(objects);
                Type = FormatType.None;
            }
        }

        if (ArrayType == ArrayTypes.FullGrid)
        {
            for (int y = 0; y < objects[0].transform.childCount; y++)
            {
                GameObject[] row = new GameObject[objects[0].transform.childCount];
                for (int i = 0; i < objects[y].transform.childCount; i++)
                {

                    row[i] = objects[y].transform.GetChild(i).gameObject;
                    row[i].GetComponent<XY>().x = i;
                    row[i].GetComponent<XY>().y = y;
                }
                if (Type != FormatType.None)
                {
                    
                    if (Type == FormatType.Position)
                    {
                        Postion();
                    }

                    if (Type == FormatType.ResetPos)
                    {
                        PosResetFull(row);
                    }

                    if (Type == FormatType.Name)
                    {
                        Name(row);
                    }

                    if (Type == FormatType.OFF)
                    {
                        OFF(row);
                    }

                    if (Type == FormatType.ON)
                    {
                        ON(row);
                    }
                    if (Type == FormatType.Visible)
                    {
                        Visible(row);
                    }
                    if (Type == FormatType.Invisible)
                    {
                        InVisible(row);
                    }
                }
            }
            Type = FormatType.None;
        }

        }

        void Postion()
    {

        for (int y = 0; y < objects.Length; y++)
        {
            objects[y].transform.localPosition = new Vector3(0, 0, SeperationDistance * y);

            for (int i = 0; i < objects[0].transform.childCount; i++)
            {
                print(i);
                objects[y].transform.GetChild(i).transform.localPosition = new Vector3(i*SeperationDistance, 0, 0);

            }
        }
 
    }

    void PosReset(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    void PosResetFull(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].transform.localPosition = new Vector3(0, 0, 0);
            objects[i].transform.localPosition = new Vector3(0, 0, 0);
        }
    }


    void Name(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].name = NameBase + (i + 1);
        }
    }

    void OFF(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].SetActive(false);
        }
    }

    void ON(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].SetActive(true);
        }
    }
    void Visible(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].GetComponent<MeshRenderer>().enabled = true;
        }
    }
    void InVisible(GameObject[] row)
    {
        for (int i = 0; i < row.Length; i++)
        {
            row[i].GetComponent<MeshRenderer>().enabled = false;
        }
    }
}
